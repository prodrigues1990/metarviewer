﻿using Domain.Factories;
using Domain.Repositories;

namespace Data
{
    public class MetarRepositoryFactory : IRepositoryFactory
    {
        public IMetarRepository MetarRepository()
        {
            return new MetarRepository();
        }
    }
}
