﻿using Domain.Entities;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Data
{
    internal class MetarRepository : IMetarRepository
    {
        private Uri NOAA_API = new Uri("https://metar-getter.herokuapp.com");
        private static readonly HttpClient httpClient = new HttpClient();

        private async Task<Metar> GetMetar(string icao)
        {
            httpClient.BaseAddress = NOAA_API;
            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("ICAO", icao)
            });
            HttpResponseMessage apiResponse = await httpClient.PostAsync("/metar", content);

            string metar = await apiResponse.Content.ReadAsStringAsync();

            return await Task.FromResult(new Metar(metar));
        }

        public async Task<IRepositoryResponse<Metar>> Get(string icao)
        {
            try
            {
                return new RepositoryResponse<Metar>(await GetMetar(icao));
            }
            catch (HttpRequestException exception)
            {
                string message = exception.Message;
                switch(message)
                {
                    case "Response status code does not indicate success: 404 (Not Found).":
                        return new UnknownIcao<Metar>(
                            string.Format("No such ICAO code {0}", icao));
                    case "An error occurred while sending the request.":
                        return new NetworkUnavailable<Metar>(
                            string.Format("Unable to reach server {0}.", NOAA_API));
                    default:
                        throw exception;
                }
            }
        }
    }
}
