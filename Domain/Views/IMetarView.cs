﻿using System;

namespace Domain.Views
{
    public interface IMetarView
    {
        string Icao { set; }
        IMetarItemView MetarView { get; }
        event EventHandler<string> IcaoChanged;
    }
}
