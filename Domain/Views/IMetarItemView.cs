﻿namespace Domain.Views
{
    public interface IMetarItemView
    {
        string RawReport { set; }
    }
}
