﻿namespace Domain.Entities
{
    public class Metar : IEntity
    {
        public string RawReport { get; }

        public Metar(string rawReport)
        {
            RawReport = rawReport;
        }
    }
}
