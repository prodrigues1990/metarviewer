﻿using Domain.Entities;

namespace Domain.Repositories
{
    public class RepositoryError<T> : IRepositoryResponse<T> where T : IEntity
    {
        public string Message { get; }

        public RepositoryError(string message)
        {
            Message = message;
        }
    }
}
