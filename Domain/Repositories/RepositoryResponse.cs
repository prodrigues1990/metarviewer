﻿using Domain.Entities;

namespace Domain.Repositories
{
    public class RepositoryResponse<T> : IRepositoryResponse<T> where T : IEntity
    {
        public T Result { get; }

        public RepositoryResponse(T result)
        {
            Result = result;
        }
    }
}
