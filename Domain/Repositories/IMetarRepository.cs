﻿using Domain.Entities;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface IMetarRepository
    {
        Task<IRepositoryResponse<Metar>> Get(string icao);
    }
}
