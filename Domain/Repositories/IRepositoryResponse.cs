﻿using Domain.Entities;

namespace Domain.Repositories
{
    public interface IRepositoryResponse<T> where T : IEntity { }
}
