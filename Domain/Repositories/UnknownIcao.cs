﻿using Domain.Entities;

namespace Domain.Repositories
{
    public class UnknownIcao<T> : RepositoryError<T> where T : Metar
    {
        public UnknownIcao(string message) : base(message) { }
    }
}
