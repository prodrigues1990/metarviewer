﻿using Domain.Entities;

namespace Domain.Repositories
{
    public class NetworkUnavailable<T> : RepositoryError<T> where T : IEntity
    {
        public NetworkUnavailable(string message) : base(message) { }
    }
}
