﻿using Domain.Presenters;
using Domain.Views;

namespace Domain.Factories
{
    internal interface IPresenterFactory
    {
        IMetarPresenter MetarPresenter(IMetarView view);
        IMetarItemPresenter MetarItemPresenter(IMetarItemView view, string icao);
    }
}
