﻿using Domain.Repositories;

namespace Domain.Factories
{
    public interface IRepositoryFactory
    {
        IMetarRepository MetarRepository();
    }
}
