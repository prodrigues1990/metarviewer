﻿using Domain.Presenters;
using Domain.Repositories;
using Domain.Views;

namespace Domain.Factories
{
    public class PresenterFactory : IPresenterFactory
    {
        private readonly IRepositoryFactory repositoryFactory;

        public PresenterFactory(IRepositoryFactory repositoryFactory)
        {
            this.repositoryFactory = repositoryFactory;
        }

        public IMetarItemPresenter MetarItemPresenter(IMetarItemView view, string icao)
        {
            IMetarRepository repository = repositoryFactory.MetarRepository();
            return new MetarItemPresenter(view, repository, icao);
        }

        public IMetarPresenter MetarPresenter(IMetarView view)
        {
            return new MetarPresenter(view, this);
        }
    }
}
