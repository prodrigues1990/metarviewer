﻿using Domain.Entities;
using Domain.Repositories;
using Domain.Views;
using System;
using System.Threading.Tasks;

namespace Domain.Presenters
{
    internal class MetarItemPresenter : IMetarItemPresenter
    {
        private const int REFRESH_RATE = 1 * 60 * 1000; // one minute
        private readonly IMetarItemView view;
        private readonly IMetarRepository repository;

        private readonly string icao;

        public MetarItemPresenter(IMetarItemView view, IMetarRepository repository, string icao)
        {
            this.view = view;
            this.repository = repository;
            this.icao = icao;
        }

        public void Start()
        {
            view.RawReport = "loading..";
            UpdateMetar();
        }

        private async void UpdateMetar()
        {
            while (true)
            {
                Handle(await repository.Get(icao));

                await Task.Delay(REFRESH_RATE);
            }
        }

        private void Handle(IRepositoryResponse<Metar> response)
        {
            if (response is RepositoryResponse<Metar>)
                Handle(response as RepositoryResponse<Metar>);
            else if (response is RepositoryError<Metar>)
                Handle(response as RepositoryError<Metar>);
        }

        private void Handle(RepositoryResponse<Metar> response)
        {
            view.RawReport = response.Result.RawReport;
        }

        private void Handle(RepositoryError<Metar> response)
        {
            view.RawReport = string.Format("Error: {0}", response.Message);
        }
    }
}
