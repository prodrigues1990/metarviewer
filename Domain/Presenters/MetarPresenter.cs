﻿using Domain.Factories;
using Domain.Views;

namespace Domain.Presenters
{
    internal class MetarPresenter : IMetarPresenter
    {
        private readonly IMetarView view;
        private readonly IPresenterFactory presenterFactory;

        private IMetarItemPresenter metarPresenter;

        public MetarPresenter(IMetarView view, IPresenterFactory presenterFactory)
        {
            this.view = view;
            this.presenterFactory = presenterFactory;

            view.IcaoChanged += (s, icao) => OnIcaoChanged(icao);
        }

        private void OnIcaoChanged(string icao)
        {
            if (icao.Length != 4)
                return;

            metarPresenter = presenterFactory.MetarItemPresenter(view.MetarView, icao);
            metarPresenter.Start();

            view.Icao = "";
        }
    }
}
