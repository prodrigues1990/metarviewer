﻿using Domain.Factories;
using Domain.Repositories;

namespace App
{
    internal class FakeRepositoryFactory : IRepositoryFactory
    {
        public IMetarRepository MetarRepository()
        {
            return new FakeMetarRepository();
        }
    }
}
