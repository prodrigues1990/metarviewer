﻿using Domain.Views;
using System.Windows.Forms;

namespace App.Views
{
    public partial class MetarItemControl : UserControl, IMetarItemView
    {
        public string RawReport { set => lblRawReport.Text = value; }

        public MetarItemControl()
        {
            InitializeComponent();
        }
    }
}
