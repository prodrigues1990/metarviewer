﻿namespace App.Views
{
    partial class MetarItemControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRawReport = new System.Windows.Forms.Label();
            this.SuspendLayout();
            //
            // lblRawReport
            //
            this.lblRawReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRawReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRawReport.Location = new System.Drawing.Point(0, 0);
            this.lblRawReport.Name = "lblRawReport";
            this.lblRawReport.Size = new System.Drawing.Size(409, 58);
            this.lblRawReport.TabIndex = 0;
            this.lblRawReport.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            //
            // MetarItemControl
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblRawReport);
            this.Name = "MetarItemControl";
            this.Size = new System.Drawing.Size(409, 58);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblRawReport;
    }
}
