﻿namespace App.Views
{
    partial class MetarForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.txtIcao = new System.Windows.Forms.ToolStripTextBox();
            this.metarItemControl = new App.Views.MetarItemControl();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            //
            // toolStrip
            //
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtIcao});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(512, 25);
            this.toolStrip.TabIndex = 0;
            this.toolStrip.Text = "toolStrip1";
            //
            // txtIcao
            //
            this.txtIcao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIcao.Name = "txtIcao";
            this.txtIcao.Size = new System.Drawing.Size(100, 25);
            //
            // metarItemControl
            //
            this.metarItemControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metarItemControl.Location = new System.Drawing.Point(0, 25);
            this.metarItemControl.Name = "metarItemControl";
            this.metarItemControl.Size = new System.Drawing.Size(512, 59);
            this.metarItemControl.TabIndex = 1;
            //
            // MetarForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 84);
            this.Controls.Add(this.metarItemControl);
            this.Controls.Add(this.toolStrip);
            this.Name = "MetarForm";
            this.Text = "MetarForm";
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripTextBox txtIcao;
        private MetarItemControl metarItemControl;
    }
}