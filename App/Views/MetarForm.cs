﻿using Domain.Views;
using System;
using System.Windows.Forms;

namespace App.Views
{
    internal partial class MetarForm : Form, IMetarView
    {
        public string Icao { set => txtIcao.Text = value; }
        public IMetarItemView MetarView => metarItemControl;
        public event EventHandler<string> IcaoChanged;

        public MetarForm()
        {
            InitializeComponent();

            txtIcao.TextChanged += (s, e) => IcaoChanged?.Invoke(this, txtIcao.Text);
        }
    }
}
