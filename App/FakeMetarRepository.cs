﻿using Domain.Entities;
using Domain.Repositories;
using System;
using System.Threading.Tasks;

namespace App
{
    internal class FakeMetarRepository : IMetarRepository
    {
        public Task<IRepositoryResponse<Metar>> Get(string icao)
        {
            IRepositoryResponse<Metar> response = new RepositoryResponse<Metar>(
                                    new Metar(new Random().Next().ToString()));
            return Task.FromResult(response);
        }
    }
}
