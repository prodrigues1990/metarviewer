﻿using App.Views;
using Data;
using Domain.Factories;
using System;
using System.Windows.Forms;

namespace App
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            IRepositoryFactory repositories = new MetarRepositoryFactory();
            PresenterFactory presenters = new PresenterFactory(repositories);
            MetarForm metarView = new MetarForm();

            presenters.MetarPresenter(metarView);

            Application.Run(metarView);
        }
    }
}
